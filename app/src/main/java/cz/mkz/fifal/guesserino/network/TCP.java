package cz.mkz.fifal.guesserino.network;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import static java.lang.Thread.sleep;

/**
 * Created by fifal on 23.2.17.
 */

public class TCP{
    private InetAddress IPAddress;
    private int port;
    private Socket socket;

    public TCP(String _IPAddress, int _port) {
        this.port = _port;
        final String ip = _IPAddress;

        try {
            IPAddress = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            return;
        }

        try {
            socket = new Socket(IPAddress, port);
            Log.i("TCP()", "Socket vytvořen");
        } catch (Exception e) {
            return;
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public String receiveMsg() {
        try {
            if (socket != null) {
                InputStream is = socket.getInputStream();InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String buffer;
                String message = "";
                while ((buffer = br.readLine()) != null) {
                    message += buffer;
                    if (message.contains(";")) {
                        return message;
                    }
                }
                return NetworkCode.PROTOCOL_PREFIX + NetworkCode.SRV_DROPPED.toString() + NetworkCode.PROTOCOL_SUFFIX;
            } else {
                //return NetworkCodes.PROTOCOL_PREFIX + NetworkCodes.getMessage(NetworkCode.SRV_NOT_FOUND) + NetworkCodes.PROTOCOL_SUFFIX;
                return "";
            }
        } catch (IOException e) {
            if (e.getMessage().equals("Connection reset")) {
                return NetworkCode.PROTOCOL_PREFIX + NetworkCode.SRV_DROPPED.toString() + NetworkCode.PROTOCOL_SUFFIX;
            }
            return "";
        }
    }

    public void sendMessage(String message){
        try {
            if (socket != null) {
                Log.i("[TCP][sendMessage]", "Odesílám zprávu: \"" + message + "\"");
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.write(message.getBytes());
                sleep(50);
            }
        } catch (IOException e) {
            Log.e("sendMessage", e.getMessage());
        } catch (InterruptedException e) {
            Log.e("sendMessage", e.getMessage());
        }
    }

    public void sendMessage(Message message){
        sendMessage(message.toString());
    }
}

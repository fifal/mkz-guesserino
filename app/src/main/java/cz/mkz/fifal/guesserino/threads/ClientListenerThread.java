package cz.mkz.fifal.guesserino.threads;

import android.graphics.drawable.Drawable;
import android.util.Log;

import cz.mkz.fifal.guesserino.config.Config;
import cz.mkz.fifal.guesserino.network.NetworkCode;
import cz.mkz.fifal.guesserino.utils.ImageUtils;

/**
 * Created by fifal on 25.2.17.
 */

public class ClientListenerThread implements Runnable {
    /**
     * Starts listener for server messages
     */
    @Override
    public void run() {
        Log.i("[CLT][run]", "Startuju listener");
        while (Config.CLIENT_LISTENER_RUNNING) {
            String message = Config.TCP_CONNECTION.receiveMsg();
            if (!message.equals("")) {
                Log.i("[CLT][run]", "Přijata zpráva: \"" + message + "\"");
            }
            if (isValidMessage(message)) {
                processMessage(message);
            }
        }
    }

    //TODO: validace zpráv
    private boolean isValidMessage(String message) {
        return true;
    }

    private void processMessage(String message) {
        message = message.replace(";", "");
        String[] splitMessage = message.split(":");
        if (splitMessage.length > 1) {
            NetworkCode networkCode = NetworkCode.getNetworkCode(splitMessage[1]);

            if (networkCode != NetworkCode.SRV_ACK
                    && networkCode != NetworkCode.SRV_NOT_VALID
                    && networkCode != NetworkCode.SRV_DROPPED) {
                try {
                    long hash = Long.parseLong(splitMessage[splitMessage.length - 1]);
                    Config.TCP_CONNECTION.sendMessage(NetworkCode.PROTOCOL_PREFIX + NetworkCode.CL_ACK.toString() + hash + NetworkCode.PROTOCOL_SUFFIX);
                } catch (Exception ex) {
                    Log.e("[CLT][processMessage]", "Špatný hash: " + ex.getMessage());
                }
            }

            switch (networkCode) {
                case SRV_CONNECTED:
                    Config.MAIN_ACTIVITY.showGameUI();
                    break;
                case SRV_INFO:
                    if (Config.GAME_ACTIVITY != null) {
                        Config.GAME_ACTIVITY.appendServerLog(splitMessage[2]);
                    }
                    break;
                case SRV_ACK:
                    long hash = Long.parseLong(splitMessage[splitMessage.length - 1]);
                    Config.MST_THREAD.ackMessage(hash);
                    break;
                case SRV_DRAW:
                    Config.GAME_ACTIVITY.setButtons(true);
                    break;
                case SRV_WORD:
                    Config.GAME_ACTIVITY.setWord("Kresli: " + splitMessage[2]);
                    Config.GAME_ACTIVITY.clearCanvas();
                    break;
                case SRV_IMAGE:
                    String imgStr = splitMessage[2];
                    byte[] imgArr = ImageUtils.getByteArrayFromString(imgStr);
                    Drawable img = ImageUtils.getImgFromByteArray(imgArr);

                    Config.GAME_ACTIVITY.setImageViewImage(img);
                    Config.GAME_ACTIVITY.setWord("Hádej slovo!");
                    break;
                case SRV_BTNS_GUESS:
                    Config.GAME_ACTIVITY.clearCanvas();
                    Config.GAME_ACTIVITY.setButtons(false);
                    Config.GAME_ACTIVITY.enableGuessButton();
                    Config.GAME_ACTIVITY.setWord("Kreslí jiný hráč.");
                    break;
                case SRV_DISCONNECT:
//                    Config.MESSAGE_SENDER_RUNNING = false;
//                    Config.GAME_ACTIVITY.finishActivity();
//                    Config.CLIENT_LISTENER_RUNNING = false;
                    break;
                case SRV_DROPPED:
                    Log.i("[CLT][processMessage]", "Ztraceno spojení se serverem");
                    Config.MESSAGE_SENDER_RUNNING = false;
                    if (Config.GAME_ACTIVITY != null) {
                        Config.GAME_ACTIVITY.finishActivity();
                        Config.MAIN_ACTIVITY.setButtons(false);
                        Config.MAIN_ACTIVITY.showAlert("Chyba!", "Ztraceno spojení se serverem", android.R.drawable.ic_delete);
                    }
                    Config.TCP_CONNECTION = null;
                    Config.CLIENT_LISTENER_RUNNING = false;
                    break;
                case SRV_BAD_NAME:
                    Log.i("[CLT][processMessage]", "Špatný nickname");
                    Config.MAIN_ACTIVITY.showToast("Nick musí být v rozsahu 2-15 písmen");
                    Config.TCP_CONNECTION = null;
                    break;
            }
        }
    }
}

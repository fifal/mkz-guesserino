package cz.mkz.fifal.guesserino.config;

import cz.mkz.fifal.guesserino.GameActivity;
import cz.mkz.fifal.guesserino.MainActivity;
import cz.mkz.fifal.guesserino.network.TCP;
import cz.mkz.fifal.guesserino.threads.MessageSenderThread;

/**
 * Created by fifal on 24.2.17.
 */

public final class Config {
    private Config(){}

    public static boolean DEBUG = true;

    public static boolean CLIENT_LISTENER_RUNNING = false;
    public static boolean MESSAGE_SENDER_RUNNING = false;

    public static MainActivity MAIN_ACTIVITY;
    public static GameActivity GAME_ACTIVITY;
    public static MessageSenderThread MST_THREAD;

    public static int CANVAS_SIZE = 864;
    public static int SERVER_IMAGE_SIZE = 364;

    public static TCP TCP_CONNECTION;
}

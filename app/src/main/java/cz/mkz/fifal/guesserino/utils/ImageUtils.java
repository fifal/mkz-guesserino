package cz.mkz.fifal.guesserino.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import cz.mkz.fifal.guesserino.config.Config;

/**
 * Created by fifal on 25.2.17.
 */

public class ImageUtils {
    /**
     * Returns Drawable as byte array
     *
     * @param drawable drawable image
     * @return byte array if successful, otherwise returns null
     */
    public static byte[] getByteArrayFromImg(Drawable drawable) {

        BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
        Bitmap bitmap = bitmapDrawable.getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(bitmap, Config.SERVER_IMAGE_SIZE, Config.SERVER_IMAGE_SIZE, false);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapResized.compress(Bitmap.CompressFormat.PNG, 100, stream);

        return stream.toByteArray();
    }

    /**
     * Returns byte array as WritableImage
     *
     * @param byteArray byte array
     * @return WritableImage if successful, otherwise returns null
     */
    public static Drawable getImgFromByteArray(byte[] byteArray) {
        return new BitmapDrawable(null, BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length));
    }

    /**
     * Encodes byteArray as a string, so we can send it through text TCP protocol
     *
     * @param imgArray byte array
     * @return String
     */
    public static String getStringFromByteArray(byte[] imgArray) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gzipOutputStream.write(imgArray);
            gzipOutputStream.finish();
            gzipOutputStream.flush();
            gzipOutputStream.close();
            byte[] array = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(array, Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Decodes string as a byte array
     *
     * @param imgString
     * @return
     */
    public static byte[] getByteArrayFromString(String imgString) {
        try {
            byte[] imgArray = Base64.decode(imgString, Base64.DEFAULT);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imgArray);
            GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);

            byte[] buffer = new byte[1024];
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            int len;
            while ((len = gzipInputStream.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }

            out.close();
            gzipInputStream.close();
            return out.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
